<?php 
    include 'presentacion/menu.php';

    $estudent = new estudiante("","","","");
    $estudiar = $estudent -> consultarTodos();
?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<div class="card-header">
					<h3>Consultar Estudiantes</h3>
				</div>
				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Codigo</th>
								<th>Nombre</th>
								<th>Apellido</th>
								<th>Fecha Nacimiento</th>
							</tr>
						</thead>
						<tbody>
						<?php 
						$i=1;
						foreach ($estudiar as $estudianteActual){
						    echo "<tr>";
						    echo "<td>" . $i++ . "</td>
                                  <td>" . $estudianteActual -> getCodigo() . "</td>
                                  <td>" . $estudianteActual -> getNombre() . "</td>
                                  <td>" . $estudianteActual -> getApellido() . "</td>
                                  <td>" . $estudianteActual -> getFechaNacimiento() . "</td>";
						    echo "</tr>";						    
						}						
						?>						
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>