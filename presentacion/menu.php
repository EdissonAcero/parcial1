<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid " style="padding-left: 3;">
    <!-- boton con icono de inicio pagina principal -->
    <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>"><i class="fas fa-home"></i></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <!-- fin --
     -- opciones para el Admin -->
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- opciones CRUD -->
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <!-- lista de opciones de CRUD para licores -->
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
           	opciones
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>">consultar</a></li>
            <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/insertar.php")?>">insertar</a></li>
          </ul>
        </li> 
    </div>
    <!-- fin -->
  </div>
</nav>