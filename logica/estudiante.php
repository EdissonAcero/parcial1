<?php 

require_once 'persistencia/Conexion.php';
require_once 'persistencia/estudianteDAO.php';

class estudiante {
    private $codigo;
    private $nombre;
    private $apellido;
    private $fechaNacimiento;
    private $conecxion;
    private $EstudianteDAO;
    
    public function getCodigo()
    {
        return $this->codigo;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getApellido()
    {
        return $this->apellido;
    }

    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    public function estudiante($pCodigo="", $pNombre="", $pApellido="", $pFechaNacimiento=""){
        $this -> codigo = $pCodigo;
        $this -> nombre = $pNombre;
        $this -> apellido = $pApellido;
        $this -> fechaNacimiento = $pFechaNacimiento;
        $this -> conecxion = new Conexion();
        $this -> EstudianteDAO = new estudianteDAO($this->codigo, $this->nombre, $this->apellido, $this->fechaNacimiento);
    }
    
    function crearAlumno() {
        $this -> conecxion -> abrir();
        $this -> conecxion -> ejecutar($this -> EstudianteDAO -> crearAlumno());
        $this -> conecxion -> cerrar();
    }
    
    function validarID (){
        $this -> conecxion -> abrir();
        $this -> conecxion -> ejecutar($this -> EstudianteDAO -> validarID());
        $this -> conecxion -> cerrar();
        if($this -> conecxion -> numFilas() == 1){
            $this -> idCliente = $this -> conecxion -> extraer()[0];
            return true;
        }else{
            return false;
        }
    }
    
    function consultarTodos() {
        $this -> conecxion -> abrir();
        $this -> conecxion -> ejecutar($this -> EstudianteDAO -> consultarTodos());
        $this -> conecxion -> cerrar();
        $estudiantes = array();
        while(($resultado = $this -> conecxion -> extraer()) != null){
            array_push($estudiantes, new estudiante($resultado[0], $resultado[1], $resultado[2], $resultado[3]));
        }
        return $estudiantes;
    }
    
}

?> 